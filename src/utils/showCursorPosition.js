const robotjs = require('robotjs');
const timeout = require('timers/promises');

async function run() {
    while(true) {
        console.log(robotjs.getMousePos().x + ", " + robotjs.getMousePos().y + " - " + robotjs.getPixelColor(robotjs.getMousePos().x,robotjs.getMousePos().y));
        await timeout.setTimeout(200);
    }
}

run().then();