/**
 * @author jmbluethner <jan-morris.bluethner@becker-avionics.com>
 * @copyright 2022 All Rights Reserved
 */

const csvtojson = require('csvtojson');
const robotjs = require('robotjs');
const fs = require('fs');
const cliProgress = require('cli-progress');
const timeout = require('timers/promises');
const auto = require('./js/autoFunctions');

/**
 * @description Defines which fields the input CSV must have in order to be considered valid
 * @type {*[]}
 */
const requiredFields = ['x','y','operation','data','delay_ms'];

/**
 * @description Reads the input CSV and converts it to JSON. Returns false if the file could not be found.
 * @return {Promise<any[]|boolean>}
 */
async function readInputFile() {

    // Check if input file is present
    try {

        if(fs.existsSync('./input/input.csv')) {
            return await csvtojson({delimiter: ';'}).fromFile('./input/input.csv');
        }

    } catch(err) {

        console.log(err);
        return false;

    }

}

/**
 * @description Datasets which can be used in the input.csv pattern
 * @return {Promise<any[]|boolean>}
 */
async function readDatasetFile() {

    try {

        if(fs.existsSync('./input/datasets.csv')) {
            return await csvtojson({delimiter: ';'}).fromFile('./input/datasets.csv');
        }

    } catch(err) {

        console.log(err);
        return false;

    }

}

/**
 * @param jsonArr
 * @return boolean
 */
function checkInputJsonFields(jsonArr) {

    /**
     *
     * @type {boolean}
     */
    let res = true;

    requiredFields.forEach((field) => {

        // console.log(Object.keys(jsonArr[0]).includes(field));

        if(!Object.keys(jsonArr[0]).includes(field)) {
            res = false;
        }

    });

    return res;

}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * @description Checks if the currently processed data value is a variable. Return unprocessed input if so. Return variable value from datasets.js instead.
 * @param dataValue
 * @param dataSet
 */
function checkDataType(dataValue,dataSet) {

    if(dataValue.substring(0,2) === '##') {
        return dataSet[dataValue.slice(2)];
    } else {
        return dataValue;
    }

}

async function run() {

    /**
     * @description Holds converted data from /input/input.csv
     * @type {*[]|boolean}
     */
    const json = await readInputFile();

    /**
     * @description Datasets which can be used in the input.csv pattern
     * @type {*[]|boolean}
     */
    const datasets = await readDatasetFile();

    if(!json) {
        console.log("/input/input.csv missing!");
        return false;
    }

    if(!checkInputJsonFields(json)) {
        console.log("Not all required fields have been supplied! Aborting.");
        return false;
    } else {
        console.log("All required fields have been supplied. Check success.");
    }

    // robotjs.moveMouse(100,100);

    console.log("Input file contains " + json.length + " datasets.");

    console.log("Waiting 10 seconds before starting the loop.");

    await timeout.setTimeout(10000);

    console.log("Starting loop process...");

    /**
     *
     * @type {cliProgress.SingleBar}
     */
    const bar1 = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);

    if(datasets.length > 0) {
        bar1.start(datasets.length, 0);
    } else {
        bar1.start(json.length, 0);
    }


    /**
     * @description temporary variable to hold the iteration counter of the coming for loop. Can't use forEach here because of the async/await statement for the timeout.
     * @type {number}
     */
    let count = 0;

    if(datasets.length > 0) {

        // Operating in dataset mode

        for(let datasetIndex in datasets) {

            count++;

            for(let index in json) {

                /* @TODO loop over the pattern steps and fill placeholder variables */

                if(json[index].operation === 'stroke') {

                    auto.stroke(checkDataType(json[index].data,datasets[datasetIndex]));

                } else if(json[index].operation === 'text') {

                    auto.text(checkDataType(json[index].data,datasets[datasetIndex]));

                } else if(json[index].operation === 'click') {

                    auto.click(json[index].x,json[index].y,json[index].data);

                } else {
                    console.log("Error. Unknown operation '" + json[index].operation + "'.");
                    return false;
                }

				await sleep(json[index].delay_ms);

            }

            bar1.update(count);

        }

    } else {

        // Operating in legacy mode

        for(let index in json) {

            count++;

            if(json[index].operation === 'stroke') {

                auto.stroke(json[index].data);

            } else if(json[index].operation === 'text') {

                auto.text(json[index].data);

            } else if(json[index].operation === 'click') {

                auto.click(json[index].x,json[index].y,json[index].data = 'single');

            } else {
                console.log("Error. Unknown operation '" + json[index].operation + "'.");
                return false;
            }

            await timeout.setTimeout(json[index].delay_ms);
            bar1.update(count);

        }

    }

    bar1.stop();

    return true;

}

run().then(r => {
    console.log("Process Done");
});