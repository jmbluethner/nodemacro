const robotjs = require('robotjs');

/**
 * @description Simulates a left mouse click
 * @param {Number} x X-Position in pixels
 * @param {Number} y Y-Position in pixels
 * @param {string} type Accepts 'double' (Double Click) or 'single' (Single Click). Defaults to 'single'.
 */
function click(x,y,type= 'single') {

    robotjs.moveMouse(x,y);

    if( type === 'single') {
        robotjs.mouseClick();
    } else if(type === 'double') {
        robotjs.mouseClick('left',true);
    }

}

/**
 * @description Simulates keyboard key presses (letters and numbers) by splitting a string into chars and then simulating every single stroke.
 * @param {string} txt
 */
function text(txt) {

    /*
    for(let char in txt) {
        robotjs.keyTap(txt[char]);
    }
     */

    robotjs.setKeyboardDelay(25);
    robotjs.typeString(txt);

}

/**
 * @description Simulates a special key press. See a table of all keys here: https://robotjs.io/docs/syntax#keys
 * @param {string} key
 */
function stroke(key) {

    robotjs.keyTap(key);

}

module.exports.stroke = stroke;
module.exports.text = text;
module.exports.click = click;