<img src="./docs/NodeMacro.svg" width="500">

<i>NodeMacro - Easily automate stupid shit</i>

<img src="https://img.shields.io/badge/Built_With-NodeJS-green" />

<img src="https://img.shields.io/badge/License-WTFPL-orange" />

<a href="http://www.wtfpl.net/">
<img height="20px" src="http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png" />
</a>

---

[[_TOC_]]

---

# About

I built NodeMacro because I had to do quite a lot of data maintenance in the process of migrating an ERP system.But entering text and clicking stuff manually sucks. Big time.  
So I came up with a dynamic solution that allows you to do go through as much data as you like - **Without having to touch any code**.

# Features

- Define Macro Patterns and dynamic datasets through CSV tables. No need for touching any code.
- Support for single keystrokes, clicks (left, right / single, double) and String input.
- Customizable delay in between actions.
- Pixel-Color checking to avoid errors that might occur from popups for example.

# Requirements

- NodeJS, developed and tested on v21.2.0^
- NPM, developed and tested on v10.5.0^

> ⚠ Since this repository requires <a href="https://robotjs.io/">RobotJS</a>, you'll have to make sure to also cover its dependencies. Otherwise `npm install`, and `npm rebuild` if required, will fail!  
> You can find all specifications here: <a href="https://robotjs.io/docs/building">https://robotjs.io/docs/building </a>

# Setup

## 1. Download

Get the current master branch source code via Git CLI  

`git clone https://gitlab.com/jmbluethner/nodemacro`

or as a .zip archive

<a href="https://gitlab.com/jmbluethner/nodemacro/-/archive/master/nodemacro-master.zip">https://gitlab.com/jmbluethner/nodemacro/-/archive/master/nodemacro-master.zip </a>

After downloading, copy or move everything from the `/src` folder, and place it wherever you want.  
If you want to skip this step because you feel cool and prefer `cd ./src` ... Okay.  

The docs will assume that all files have been extracted from `/src` from now on.

## 2. Install  dependencies

`npm install` in your source folder.

> ⚠ As described earlier, make sure that you also cover the <a href="https://robotjs.io/">RobotJS</a> requirements.

## 3. Add Data

In order to tell NodeMacro what to do, you need to create two files:  
`datasets.csv` and `input.csv`. In case you're not familiar with CSV, which you most likely are since you're looking this shit up on GitLab, don't worry, it's easy and can be done with common tools like LibreOffice Calc or Excel.  

Both these files must be placed in the `input` folder.

### 3.1. Datasets

#### General

`datasets.csv` defines ... Well, your datasets.
Let's assume you have to enter dynamic text in an input field. In that case, you would put all your text in the `datasets.csv`, so that NodeMacro can iterate over all lines of said table.  

`datasets.csv` also defines the amount of iterations you want to run. So, even if you want to repeat the same action over and over again, without making use of any dynamic data, you'll still need to add X amount of lines to `datasets.csv`.  

If you want to run 100 iterations, give `datasets.csv` at least one column and 100 rows.  
Column identifiers can have completely custom names, just make sure to avoid special characters and that those names are unique.

#### Layout

| [IDENTIFIER]     |
|------------------|
| Value (_String_) |

#### Example

| MY_TEXT_1               | MY_TEXT_2               | MY_TEXT_3               |
|-------------------------|-------------------------|-------------------------|
| Enter this text         | Than enter this         | And now enter this      |
| Second iteration text 1 | Second iteration text 2 | Second iteration text 3 |

### 3.2. Input

#### General

After defining what data you want to process in the loop we're now defining, it's time to think about what your macro has to do in terms of inputs.  

NodeMacro supports the following input types:

##### Input Types

###### Click

Sends a left mouse click.

Requires X/Y: Yes  
Requires data: Yes  
Allowed Data: "single" / "double"

###### Stroke

Sends a single key stroke.

Requires X/Y: No  
Requires data: Yes  
Allowed Data: See <a href="https://robotjs.io/docs/syntax#keys">https://robotjs.io/docs/syntax#keys </a>

###### Text

Used for sending Strings.

Requires X/Y: No  
Requires data: Yes  
Allowed data: Any amount of ASCII characters.

##### Layout

| x                | y                | operation  | data                                                                                              | rgb                                                                                                                                                                                                                     | delay_ms (defaults to 50)                    |
|------------------|------------------|------------|---------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------|
| X coords (_Int_) | X coords (_Int_) | Input Type | mapping to data from `datasets.csv` or static values. Mappings have to start with two leading `#` | Expected RGB value at current cursor position as HEX Number without leading #. If the expected RGB value is not equal to the actual value, NodeMacro will abort operation. Providing this parameter is always optional. | Delay after this step in ms. Defaults to 50. |


> Now you might be wondering how you're supposed to get the correct X and Y coordinates for click operations. Luckily, there's a small tool that can help you with that. Scroll down a bit to the **Utils** / **Get Cursor Position** section to read more.

#### Example

_This example assumes a scenario in which we're putting text into three input fields, using the data from the `datasets.csv` example shown above._

| x | y  | operation | data        | rgb | delay_ms (defaults to 50) |
|--|----|-----------|-------------|-----|---------------------------|
| 10 | 10 | click     | single      |     | 50 |
|  |    | text      | ##MY_TEXT_1 |     | 50 |
| 10 | 30 | click     | single      |     | 50 |
|  |    | text      | ##MY_TEXT_2 |     | 50 |
| 10 | 50 | click     | single      |     | 50 |
|  |    | text      | ##MY_TEXT_3 |     | 50 |

## 4. Run NodeMacro

`node app.js`

# Utils

## Get Cursor Position

In order to obtain the correct X and Y coordinates for operations that require them, such as clicking, there's a utility skript that can help you.  

If you run `node ./utils/showCursorPosition.js`, you'll get the current position of your cursor in the terminal.  

The recommended way of using this tool is as follows:

1. Run the script.
2. Move your mouse to the desired position, but make sure not to click anywhere to keep the terminal active.
3. Hit `Ctrl + C` on your keyboard to stop the script.
4. The coordinates printed last will give you the coordinates you need.

If you, for whatever reason, can't keep the terminal window active, which means that hitting `Ctrl + C` isn't an option, just hover above your desired position for a few seconds and than quickly stop the script by tabbing back into the terminal.  
That way you can easily identify the coordinates in question because they will have been printed multiple times.

# Roadmap / In the making

- Add support for conditional steps based on RGB pixel color values.
- Support for X/Y cursor coordinates defined in datasets.csv
- Create a web interface for remote macro operation monitoring (will regret later ...)

---

Developed with ♥ and a metric shit ton of ☕ by jmbluethner